package org.amdatu.bootstrap.plugins.dependencymanager;

import java.nio.file.Path;

import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.questions.FullyQualifiedName;

public interface DmService {
	InstallResult installAnnotationProcessor();
	InstallResult addDependencies();
	void createActivator(FullyQualifiedName fqn, FullyQualifiedName interfaceName);
	InstallResult addRunDependencies(Path runFilePath);
}
