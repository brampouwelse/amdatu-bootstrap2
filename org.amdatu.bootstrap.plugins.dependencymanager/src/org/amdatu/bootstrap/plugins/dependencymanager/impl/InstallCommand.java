package org.amdatu.bootstrap.plugins.dependencymanager.impl;

import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.ChoiceQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class InstallCommand implements Command<InstallResult> {

	private static final String USE_ANNOTATIONS = "useAnnotations";
	
	@ServiceDependency
	private volatile DmService m_dmService;
	

	@Override
	public String getName() {
		return "install";
	}

	@Override
	public Plugin getPlugin() {
		return DmPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.PROJECT;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(
				new ChoiceQuestion<Boolean>(USE_ANNOTATIONS, "Do you want to use annotations?",
						Arrays.asList(true, false), 0));
	}

	@Override
	public InstallResult execute(String... args) {
		Answers ansers = Answers.parse(getQuestions(), args);
		boolean useAnnotations = ansers.get(USE_ANNOTATIONS);
		
		InstallResult annotationsInstallResult = null;
		if(useAnnotations) {
			annotationsInstallResult = m_dmService.installAnnotationProcessor();
		}
		
		return InstallResult.builder()
				.addResult(annotationsInstallResult)
				.addResult(m_dmService.addDependencies()).build();
	}

}
