package org.amdatu.bootstrap.plugins.version.impl;

import org.amdatu.bootstrap.core.Plugin;

public class VersionPlugin implements Plugin{

	public static final VersionPlugin INSTANCE = new VersionPlugin();
	
	private VersionPlugin() {
		//singleton
	}
	
	@Override
	public String getName() {
		return "version";
	}

	@Override
	public String getAuthor() {
		return "Amdatu";
	}

	@Override
	public String getGitUrl() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getWebsite() {
		// TODO Auto-generated method stub
		return null;
	}

}
