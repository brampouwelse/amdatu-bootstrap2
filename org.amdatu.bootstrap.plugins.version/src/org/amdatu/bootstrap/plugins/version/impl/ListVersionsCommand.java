package org.amdatu.bootstrap.plugins.version.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.amdatu.bootstrap.core.AnswerParser;
import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.core.questions.StringQuestion;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.Workspace;
import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

@Component
public class ListVersionsCommand implements Command<List<Version>>{

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile AnswerParser m_answerParser;
	
	@Override
	public String getName() {
		return "list";
	}

	@Override
	public Plugin getPlugin() {
		return VersionPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.WORKSPACE;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(new Question<?>[]{new StringQuestion(Question.DEFAULT_KEY, "bsn?")});
	}

	@Override
	public List<Version> execute(String... args) {
		
		Answers answers = m_answerParser.parse(getQuestions(), args);
		String bsn = answers.get(Question.DEFAULT_KEY);
		
		
		SortedSet<Version> versions = new TreeSet<>();
		Workspace bndWorkspace = m_navigator.getCurrentWorkspace();
		for (RepositoryPlugin repositoryPlugin : bndWorkspace.getRepositories()) {
			try {
				versions.addAll(repositoryPlugin.versions(bsn));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		return new ArrayList<Version>(versions);
	}

}
