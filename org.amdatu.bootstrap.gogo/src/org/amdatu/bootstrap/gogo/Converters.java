package org.amdatu.bootstrap.gogo;

import java.io.File;

import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.InstallResult;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.service.command.Converter;

@Component
public class Converters implements Converter {

	@Override
	public Object convert(Class<?> desiredType, final Object in) throws Exception {
		return null;
	}

	@Override
	public CharSequence format(Object target, int level, Converter converter) throws Exception {
		if (target instanceof File) {
			return ((File) target).getPath();
		} else if(target instanceof Command) {
			return ((Command<?>)target).getFullName();
		} else if(target instanceof InstallResult) {
			InstallResult result = (InstallResult)target;
			return result.toString();
		}
		
		return null;
	}
}
