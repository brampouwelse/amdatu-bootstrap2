package org.amdatu.bootstrap.gogo;

import java.util.stream.Stream;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.gogo.api.CommandSessionListener;
import org.apache.felix.service.command.CommandProcessor;
import org.apache.felix.service.command.CommandSession;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

@Component
public class SessionListener implements CommandSessionListener {

	@Inject
	private volatile BundleContext m_bundleContext;
	
	@Override
	public void beforeExecute(CommandSession session, CharSequence command) {
			
	}

	@Override
	public void afterExecute(CommandSession session, CharSequence command,
			Exception exception) {

		if (exception.getClass().getName().equals("org.apache.felix.gogo.runtime.CommandNotFoundException")){
		
			String[] parts = command.toString().split(" ");
			String cmdName = parts[0];
			
			if (cmdName.contains(":")){
				return; //This is a scoped command don't need to create an alias
			}

			try {
				String filter = String.format("(%s=*)",CommandProcessor.COMMAND_SCOPE);
				ServiceReference<?>[] serviceReferences = m_bundleContext.getServiceReferences((String)null, filter);
				
	
				if (serviceReferences.length > 0){
					System.out.println("Did you mean: ");
					for (ServiceReference<?> ref : serviceReferences) {
						String scope = (String) ref.getProperty(CommandProcessor.COMMAND_SCOPE);
						Object functionProp = ref.getProperty(CommandProcessor.COMMAND_FUNCTION);
						if (functionProp instanceof String[]){
							Stream.of(((String[])functionProp))
								.filter(f-> f.trim().equals(cmdName))
								.forEach(f -> System.out.println("  " + String.format("%s:%s", scope, f)));	
						}else if (functionProp.equals(cmdName)){
							System.out.println("  " + String.format("%s:%s", scope, cmdName));
						}
					}
				}
				
			} catch (InvalidSyntaxException e) {
				//
			}
		}
		
	}

	@Override
	public void afterExecute(CommandSession session, CharSequence command,
			Object result) {
		
	}

}
