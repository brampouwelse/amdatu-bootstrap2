package org.amdatu.bootstrap.gogo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.ServiceReference;

@org.apache.felix.dm.annotation.api.Component
public class GogoCommandRegistry {
	
	@Inject
	private volatile DependencyManager m_dependencyManager;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	private final Map<ServiceReference<Command<?>>, org.apache.felix.dm.Component> m_commands = new ConcurrentHashMap<>();
	private final Map<String, DependencyManager> m_dependencyManagers = new ConcurrentHashMap<>();

	private final Map<ServiceReference<Command<?>>, org.apache.felix.dm.Component> m_catchAllcommands = new ConcurrentHashMap<>();
	
	private final CatchAll m_catchAll = new CatchAll();
	
	
	@ServiceDependency(removed="removeCommand")
	public void addCommand(ServiceReference<Command<?>> sr, Command<?> command) throws Exception {
		Properties props = new Properties();
		props.put(CommandProcessor.COMMAND_SCOPE, command.getPlugin().getName());
		props.put(CommandProcessor.COMMAND_FUNCTION, command.getName());

		String bsn = sr.getBundle().getSymbolicName();
		DependencyManager dm = m_dependencyManagers.getOrDefault(bsn, new DependencyManager(sr.getBundle().getBundleContext()));
		Component component = dm.createComponent().setInterface(Object.class.getName(), props).setImplementation(new CommandWrapper(command))
				.add(dm.createServiceDependency().setService(Navigator.class).setRequired(true));
		
		Properties caProps = new Properties();
		caProps.put(CommandProcessor.COMMAND_SCOPE, "catchall");
		caProps.put(CommandProcessor.COMMAND_FUNCTION, command.getName());
		Component caComponent = dm.createComponent().setInterface(Object.class.getName(), caProps).setImplementation(m_catchAll);
		
		m_commands.put(sr, component);
		m_catchAllcommands.put(sr, caComponent);
		
		dm.add(component);
		dm.add(caComponent);
		m_dependencyManagers.putIfAbsent(bsn, dm);
	}

	public void removeCommand(ServiceReference<Command<?>> sr, Command<?> command) {
		String bsn = sr.getBundle().getSymbolicName();
		
		Component component = m_commands.remove(sr);
		DependencyManager dm = m_dependencyManagers.remove(bsn);
		dm.remove(component);
		Component caComponent = m_commands.remove(sr);
		dm.remove(caComponent);
	}
	
	
	class CatchAll {
		
		public Object _main(String... args){
			String function = args[0];
			
			Command<?> cmd = null;
			
			for (Component component : m_commands.values()) {
				Command<?> c = (Command<?>) component.getService();
			
				if (c.getName().equals(function)){
					if (cmd != null){
						return null; //
					}
					cmd = c;
				}
			}
			if (cmd == null){
				return null;
			}
			
			if (Scope.PROJECT.equals(cmd.getScope()) && m_navigator.getProjectDir() == null){
				System.out.println("Command '" + cmd.getFullName() + "' can only be used in a project" );
				return null;
			}else if (Scope.WORKSPACE.equals(cmd.getScope()) && m_navigator.getWorkspaceDir() == null){
				System.out.println("Command '" + cmd.getFullName() + "' can only be used in a workspace" );
				return null;
			}
			return cmd._main(args);
		}
		
	}
	
	
	@SuppressWarnings("rawtypes")
	class CommandWrapper implements Command {
		
		private volatile Navigator m_navigator;
		
		private Command m_command;
		
		public CommandWrapper(Command command) {
			this.m_command = command;
		}

		public String getName() {
			return m_command.getName();
		}

		public Plugin getPlugin() {
			return m_command.getPlugin();
		}

		public Scope getScope() {
			return m_command.getScope();
		}

		public List getQuestions() {
			return m_command.getQuestions();
		}

		public Object execute(String... args) {
			
			if (Scope.PROJECT.equals(getScope()) && m_navigator.getProjectDir() == null){
				System.out.println("Command '" + getFullName() + "' can only be used in a project" );
				return null;
			}else if (Scope.WORKSPACE.equals(getScope()) && m_navigator.getWorkspaceDir() == null){
				System.out.println("Command '" + getFullName() + "' can only be used in a workspace" );
				return null;
			}
			
			return m_command.execute(args);
		}
		
	}
	
}
