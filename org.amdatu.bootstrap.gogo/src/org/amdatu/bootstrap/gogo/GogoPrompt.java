/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.gogo;

import java.util.List;
import java.util.Scanner;

import org.amdatu.bootstrap.core.Prompt;
import org.amdatu.bootstrap.core.questions.BooleanQuestion;
import org.amdatu.bootstrap.core.questions.ChoiceQuestion;
import org.amdatu.bootstrap.core.questions.FqnQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.gogo.api.CommandSessionListener;
import org.apache.felix.service.command.CommandSession;

@Component
public class GogoPrompt implements Prompt, CommandSessionListener{
	
	private volatile CommandSession m_session;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void ask(Question<?> question){
		
		String answer = null;
		if (question instanceof BooleanQuestion){
			answer = Boolean.toString(askBoolean(question.getLabel(), false));
		}else if (question instanceof FqnQuestion){
			answer = askString(question.getLabel(), "example.Example").toString();
		}else if (question instanceof ChoiceQuestion<?>){
			answer = Integer.toString(askChoiceAsIndex(question.getLabel(), 0, ((ChoiceQuestion) question).getOptions()));
		}else {
			answer = askString(question.getLabel(), null).toString();
		}
		question.setAnswerAsString(answer);
		
	}
	
	private boolean askBoolean(String message, boolean defaultChoice) {
		if(defaultChoice) {
			System.out.print(message + ": [Y/n]");
		} else {
			System.out.print(message + ": [y/N]");
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();
		if(answer != null && answer.length() > 0) {
			return answer.equalsIgnoreCase("y");
		} else {
			return defaultChoice;
		}
	}

	private String askString(String message, String defaultString) {
		if(defaultString != null) {
			System.out.print(message + ": [" + defaultString +"]");
		} else {
			System.out.print(message + ": ");
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();
		if(answer != null && answer.length() > 0) {
			return answer;
		} else if(defaultString != null) {
			return defaultString;
		} else {
			return askString(message, null);
		}
	}
	
	private int askChoiceAsIndex(String message, int defaultOption, @SuppressWarnings("rawtypes") List options) {
		if(options.size() == 0) {
			throw new IllegalArgumentException("No options specified");
		}
		
		System.out.println(message + ":");
		
		for(int i=0; i < options.size(); i++) {
			if(defaultOption == i) {
				System.out.println(String.format("[%d]* - %s", i, options.get(i)));
			} else {
				System.out.println(String.format("[%d] - %s", i, options.get(i)));
			}
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();
		try {
			int answerInt = Integer.parseInt(answer);
			if(answerInt < options.size()) {
				return answerInt;
			}
		} catch(NumberFormatException e) {
			//Recover by returning default option
		}
		
		return defaultOption;
	}

	
	@Override
	public void afterExecute(CommandSession arg0, CharSequence arg1, Exception arg2) {
		//TODO: This is not possible, due to the way currently aliases work, after is called after creating the alias
//		m_session = null;
	}

	@Override
	public void afterExecute(CommandSession arg0, CharSequence arg1, Object arg2) {
		
//		m_session = null;
	}

	@Override
	public void beforeExecute(CommandSession session, CharSequence arg1) {
		System.out.println(session);
		m_session = session;
	}

}
