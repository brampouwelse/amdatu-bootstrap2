package org.amdatu.bootstrap.plugins.amdatu.mongo;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.RequiredDefaultArgumentNotSet;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.testutils.mockito.matchers.DependenciesMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RunMongoCommandTest {
	@InjectMocks @Spy
	private MongoRunConfigCommand m_command = new MongoRunConfigCommand();
	 
	@Mock 
	private DependencyBuilder m_dependencyBuilder;
	
	@Mock
	private Navigator m_navigator;
	
	@Mock
	private DmService m_dmService;
	
	@SuppressWarnings("unchecked")
	@Before
	public void setup() {
		when(m_navigator.getCurrentDir()).thenReturn(Paths.get("test"));
		when(m_dependencyBuilder.addRunDependency(anyCollection(), any(Path.class))).thenReturn(InstallResult.empty());
	}
	
	@Test
	public void addWithoutArgs() {
		m_command.execute("test.bndrun");
		
		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo",
				"com.fasterxml.jackson.core.jackson-core",
				"com.fasterxml.jackson.core.jackson-databind",
				"com.fasterxml.jackson.core.jackson-annotations", 
				"org.mongojack");
		
		verifyAdd(expected);
	}
	
	@Test
	public void addWithougMapper() {
		m_command.execute("-mapper", "2","test.bndrun");
		
		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo");
		
		verifyAdd(expected);
	}
	
	@Test
	public void addWithJongo() {
		m_command.execute("-mapper", "1", "test.bndrun");
		
		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.jongo", 
				"de.undercouch.bson4jackson");
		
		verifyAdd(expected);
	}
	
	@Test
	public void addWithJongoByName() {
		m_command.execute("-mapper", "Jongo", "test.bndrun");
		
		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.jongo", 
				"de.undercouch.bson4jackson");
		
		verifyAdd(expected);
	}

	@Test
	public void addWithMongoJackByName() {
		m_command.execute("-mapper", "MongoJack", "test.bndrun");

		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo",
				"com.fasterxml.jackson.core.jackson-core",
				"com.fasterxml.jackson.core.jackson-databind",
				"com.fasterxml.jackson.core.jackson-annotations", 
				"org.mongojack");

		verifyAdd(expected);
	}

	private void verifyAdd(Set<String> expected) {
		verify(m_dependencyBuilder).addRunDependency(Mockito.argThat(new DependenciesMatcher(expected)), eq(Paths.get("test", "test.bndrun")));
	}
	
	@Test(expected=RequiredDefaultArgumentNotSet.class)
	public void errorWithoutRunFileArg() {
		m_command.execute();
	}
	
}
