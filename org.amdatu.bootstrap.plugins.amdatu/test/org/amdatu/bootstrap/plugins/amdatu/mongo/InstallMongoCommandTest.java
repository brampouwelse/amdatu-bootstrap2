package org.amdatu.bootstrap.plugins.amdatu.mongo;

import static org.mockito.Mockito.verify;

import java.util.Set;

import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.testutils.mockito.matchers.DependenciesMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InstallMongoCommandTest {

	@InjectMocks @Spy
	private InstallMongoCommand m_command = new InstallMongoCommand();
	 
	@Mock 
	private DependencyBuilder m_dependencyBuilder;
	
	@Test
	public void installWithougArgs() {
		m_command.execute();
		
		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo",
				"com.fasterxml.jackson.core.jackson-core",
				"com.fasterxml.jackson.core.jackson-databind",
				"com.fasterxml.jackson.core.jackson-annotations", 
				"org.mongojack");
		
		verify(m_dependencyBuilder).addDependencies(Mockito.argThat(new DependenciesMatcher(expected)));
	}
	
	@Test
	public void installWithougMapper() {
		m_command.execute("-mapper", "2");
		
		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo");
		
		verify(m_dependencyBuilder).addDependencies(Mockito.argThat(new DependenciesMatcher(expected)));
	}
	
	@Test
	public void installWithJongo() {
		m_command.execute("-mapper", "1");
		
		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.jongo", 
				"de.undercouch.bson4jackson");
		
		verify(m_dependencyBuilder).addDependencies(Mockito.argThat(new DependenciesMatcher(expected)));
	}
	
	@Test
	public void installWithJongoByName() {
		m_command.execute("-mapper", "Jongo");
		
		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.jongo", 
				"de.undercouch.bson4jackson");
		
		verify(m_dependencyBuilder).addDependencies(Mockito.argThat(new DependenciesMatcher(expected)));
	}

	@Test
	public void installWithMongoJackByName() {
		m_command.execute("-mapper", "MongoJack");

		Set<String> expected = com.google.common.collect.Sets.newHashSet(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo",
				"com.fasterxml.jackson.core.jackson-core",
				"com.fasterxml.jackson.core.jackson-databind",
				"com.fasterxml.jackson.core.jackson-annotations", 
				"org.mongojack");

		verify(m_dependencyBuilder).addDependencies(Mockito.argThat(new DependenciesMatcher(expected)));
	}
}
