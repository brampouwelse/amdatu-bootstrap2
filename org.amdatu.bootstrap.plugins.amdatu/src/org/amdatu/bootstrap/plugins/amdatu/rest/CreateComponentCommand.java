package org.amdatu.bootstrap.plugins.amdatu.rest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.ChoiceQuestion;
import org.amdatu.bootstrap.core.questions.FqnQuestion;
import org.amdatu.bootstrap.core.questions.FullyQualifiedName;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.core.questions.StringQuestion;
import org.amdatu.bootstrap.plugins.amdatu.AmdatuPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component
public class CreateComponentCommand implements Command<File>{

	private volatile BundleContext m_bundleContext;
	
	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile DmService m_dmService;
	
	@Override
	public String getName() {
		return "rest-create-component";
	}

	@Override
	public Plugin getPlugin() {
		return AmdatuPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.PROJECT;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(
				new FqnQuestion("fqn", "Fully qualified component name", new FullyQualifiedName("example.ExampleResource")),
				new StringQuestion("path", "Path", "example"),
				new ChoiceQuestion<String>("registrationType", "How do you want to register the component?", Arrays.asList("Annotations", "Activator"), 0));
	}

	@Override
	public File execute(String... args) {
		Answers answers = Answers.parse(getQuestions(), args);

		FullyQualifiedName fqn = answers.get("fqn");
		String path = answers.get("path");
		boolean useAnnotations = answers.get("registrationType").equals("Annotations");
		

		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/restcomponent.vm");
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("packageName", fqn.getPackageName());
			context.put("useAnnotations", useAnnotations);
			context.put("path", path);

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src")
					.resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fqn.getClassName() + ".java");
			Files.createDirectories(outputDir);
			
			Files.write(outputFile, template.getBytes());
			
			 if (useAnnotations) {
				 m_dmService.installAnnotationProcessor();
			 } else {
				 m_dmService.createActivator(fqn, new FullyQualifiedName("java.lang.Object"));
			 }
			 
			 m_dmService.addDependencies();
			
			return outputFile.toFile();
		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}

}
