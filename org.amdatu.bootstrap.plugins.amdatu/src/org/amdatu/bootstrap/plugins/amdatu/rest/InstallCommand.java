package org.amdatu.bootstrap.plugins.amdatu.rest;

import java.util.List;

import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.plugins.amdatu.AmdatuPlugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class InstallCommand implements Command<InstallResult>{

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@Override
	public String getName() {
		return "rest-install";
	}

	@Override
	public Plugin getPlugin() {
		return AmdatuPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.PROJECT;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return null;
	}

	@Override
	public InstallResult execute(String... args) {
		return installDependencies();
	}
	
	public InstallResult installDependencies() {
		return m_dependencyBuilder.addDependencies(
				Dependency.fromStrings(
						"org.amdatu.web.rest.jaxrs", 
						"org.amdatu.web.rest.doc", 
						"javax.servlet", 
						"jackson-core-asl",
						"jackson-mapper-asl"));
	}

}
