package org.amdatu.bootstrap.plugins.amdatu.mongo;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.InstallResult.Builder;
import org.amdatu.bootstrap.core.questions.ChoiceQuestion;
import org.amdatu.bootstrap.core.questions.PathQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.plugins.amdatu.AmdatuPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class MongoRunConfigCommand implements Command<InstallResult> {

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile DmService m_dmService;
	
	@Override
	public String getName() {
		return "mongo-run";
	}

	@Override
	public Plugin getPlugin() {
		return AmdatuPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.PROJECT;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(
				new PathQuestion(m_navigator.getCurrentDir(), Question.DEFAULT_KEY, "Path to run file"),
				new ChoiceQuestion<>("mapper", "Which mapper do you want to use?", 
						Arrays.asList("MongoJack", "Jongo", "None"), 0) );
	}

	@Override
	public InstallResult execute(String... args) {
		Answers answers = Answers.parse(getQuestions(), args);
		Path runFilePath = answers.get(Question.DEFAULT_KEY);
		
		Builder builder = InstallResult.builder();
		
		List<Dependency> dependencies = Dependency.fromStrings(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo",
				"org.apache.felix.configadmin");
		
		String mapper = answers.get("mapper");
		MapperDependencies.addMapperDependencies(dependencies, mapper);
		
		InstallResult installResult = m_dependencyBuilder.addRunDependency(dependencies, runFilePath);
		
		builder.addResult(installResult);
		builder.addResult(m_dmService.addRunDependencies(runFilePath));
		
		return builder.build();
	}
}
