package org.amdatu.bootstrap.plugins.amdatu.mongo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.ChoiceQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.plugins.amdatu.AmdatuPlugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class InstallMongoCommand implements Command<InstallResult>{

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@Override
	public String getName() {
		return "mongo-install";
	}

	@Override
	public Plugin getPlugin() {
		return AmdatuPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.PROJECT;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(new ChoiceQuestion<>("mapper", "Which mapper do you want to use?", 
				Arrays.asList("MongoJack", "Jongo", "None"), 0));
	}

	@Override
	public InstallResult execute(String... args) {
		List<Dependency> dependencies = new ArrayList<>();
		dependencies.addAll(Dependency.fromStrings(
						"org.mongodb.mongo-java-driver", 
						"org.amdatu.mongo"));
		
		Answers answers = Answers.parse(getQuestions(), args);
		String mapper = answers.get("mapper");
		MapperDependencies.addMapperDependencies(dependencies, mapper);
		
		return m_dependencyBuilder.addDependencies(dependencies);
	}
}
