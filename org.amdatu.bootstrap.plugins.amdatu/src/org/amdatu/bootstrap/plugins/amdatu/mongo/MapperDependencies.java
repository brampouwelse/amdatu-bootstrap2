package org.amdatu.bootstrap.plugins.amdatu.mongo;

import java.util.List;

import org.amdatu.bootstrap.core.Dependency;

public class MapperDependencies {
	public static void addMapperDependencies(List<Dependency> dependencies, String mapper) {
		switch(mapper) {
		case "MongoJack":
			dependencies.addAll(Dependency.fromStrings(
					"com.fasterxml.jackson.core.jackson-core;version='[2.3.1,2.3.1]'",
					"com.fasterxml.jackson.core.jackson-databind;version='[2.3.1,2.3.1]'",
					"com.fasterxml.jackson.core.jackson-annotations;version='[2.3.0,2.3.0]'", 
					"org.mongojack;version=2.1.0.SNAPSHOT"));
			break;
			
		case "Jongo":
			dependencies.addAll(Dependency.fromStrings(
					"org.jongo", 
					"de.undercouch.bson4jackson"));
			break;
		}
	}
}
