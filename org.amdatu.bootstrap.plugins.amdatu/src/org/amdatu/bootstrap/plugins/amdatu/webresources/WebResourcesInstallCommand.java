package org.amdatu.bootstrap.plugins.amdatu.webresources;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.ResourceManager;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.PathQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.core.questions.StringQuestion;
import org.amdatu.bootstrap.plugins.amdatu.AmdatuPlugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.properties.Document;

@Component
public class WebResourcesInstallCommand implements Command<InstallResult>{

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@Override
	public String getName() {
		return "webresources-install";
	}

	@Override
	public Plugin getPlugin() {
		return AmdatuPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.PROJECT;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(
				new StringQuestion("dir", "Directory containing the static resources"),
				new StringQuestion("path", "Path to register"),
				new StringQuestion("defaultpage", "Directory containing the static resources", ""),
				new PathQuestion(m_navigator.getCurrentDir(), Question.DEFAULT_KEY, "Path to bnd file", m_navigator.getCurrentDir().resolve("bnd.bnd")));
	}

	@Override
	public InstallResult execute(String... args) {
		Answers answers = Answers.parse(getQuestions(), args);
		String dir = answers.get("dir");
		String path = answers.get("path");
		String defaultpage = answers.get("defaultpage");
		Path bndFile = answers.get(Question.DEFAULT_KEY);
		
		addWebResources(path, defaultpage, dir, bndFile);
		
		
		return InstallResult.empty();
	}
	
	private void addWebResources(String path, String defaultPath, String resourcesDir, Path bndFile) {
		if (!path.startsWith("/")) {
			path = "/" + path;
		}

		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(bndFile.toFile());

			model.addIncludeResource(resourcesDir + "=" + resourcesDir);
			model.genericSet("X-Web-Resource-Version", " 1.1");

			model.genericSet("X-Web-Resource", createWebResourceHeader(resourcesDir, path, model));

			if (!defaultPath.equals("")) {
				model.genericSet("X-Web-Resource-Default-Page", " " + defaultPath);
			}

			String bndContents = new String(Files.readAllBytes(bndFile));
			Document document = new Document(bndContents);
			model.saveChangesTo(document);

			m_resourceManager.writeFile(bndFile, document.get().getBytes());
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	private String createWebResourceHeader(String resourcesDir, String path, BndEditModel model) {
		Object webResource = model.genericGet("X-Web-Resource");

		StringBuilder webResourceBuilder = new StringBuilder(" ");
		if (webResource != null) {
			webResourceBuilder.append(webResource).append(";");
		}

		webResourceBuilder.append(path);
		webResourceBuilder.append(";");
		webResourceBuilder.append(resourcesDir);
		return webResourceBuilder.toString();
	}

}
