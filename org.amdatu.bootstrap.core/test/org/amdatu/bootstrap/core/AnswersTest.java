package org.amdatu.bootstrap.core;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.questions.BooleanQuestion;
import org.amdatu.bootstrap.core.questions.PathQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.core.questions.StringQuestion;
import org.junit.Test;

public class AnswersTest {
	
	@Test
	public void defaultArg() {
		Answers answers = Answers.parse(Arrays.asList(new StringQuestion(Question.DEFAULT_KEY, "test")), "abc");
		String val = answers.get(Question.DEFAULT_KEY);
		assertThat(val, is("abc"));
	}

	@Test
	public void defaultArgWithDefaultValue() {
		Answers answers = Answers.parse(Arrays.asList(new StringQuestion(Question.DEFAULT_KEY, "test", "d")), "");
		String val = answers.get(Question.DEFAULT_KEY);
		assertThat(val, is("d"));
	}
	
	@Test(expected=RequiredDefaultArgumentNotSet.class)
	public void defaultArgNotSet() {
		Answers answers = Answers.parse(Arrays.asList(new StringQuestion(Question.DEFAULT_KEY, "test")), "");
		String val = answers.get(Question.DEFAULT_KEY);
		assertThat(val, is(""));
	}
	
	@Test
	public void booleanNamedArgEnabled() {
		Answers answers = Answers.parse(Arrays.asList(new BooleanQuestion("a", "test")), "-a");
		boolean val = answers.get("a");
		assertThat(val, is(true));
	}
	
	@Test
	public void booleanNamedArgDisabled() {
		Answers answers = Answers.parse(Arrays.asList(new BooleanQuestion("a", "test", false)), "");
		boolean val = answers.get("a");
		assertThat(val, is(false));
	}
	
	@Test
	public void stringNamedArgWithValue() {
		Answers answers = Answers.parse(Arrays.asList(new StringQuestion("a", "test")), "-a test");
		String val = answers.get("a");
		assertThat(val, is("test"));
	}
	
	@Test(expected=ParseExceptionWrapper.class)
	public void stringNamedArgWithoutValue() {
		Answers answers = Answers.parse(Arrays.asList(new StringQuestion("a", "test")), "");
		String val = answers.get("a");
		assertThat(val, is("test"));
	}
	
	@Test
	public void stringNamedArgWithDefaultValue() {
		Answers answers = Answers.parse(Arrays.asList(new StringQuestion("a", "test", "default")), "");
		String val = answers.get("a");
		assertThat(val, is("default"));
	}
	
	@Test
	public void stringNamedArgWithDefaultValueAndSet() {
		Answers answers = Answers.parse(Arrays.asList(new StringQuestion("a", "test", "default")), "-a test");
		String val = answers.get("a");
		assertThat(val, is("test"));
	}
	
	@Test
	public void multipleNamed() {
		Answers answers = Answers.parse(Arrays.asList(
				new StringQuestion("a", "test", "default"), 
				new BooleanQuestion("b", "test2")), "-a test", "-b");
		String val = answers.get("a");
		assertThat(val, is("test"));
		
		boolean val2 = answers.get("b");
		assertThat(val2, is(true));
	}
	
	@Test
	public void multipleNamedAndDefaultArg() {
		Answers answers = Answers.parse(Arrays.asList(
				new StringQuestion("a", "test", "default"), 
				new BooleanQuestion("b", "test2"), 
				new StringQuestion(Question.DEFAULT_KEY, "test3")), "-a test", "-b", "testarg");
		String val = answers.get("a");
		assertThat(val, is("test"));
		
		boolean val2 = answers.get("b");
		assertThat(val2, is(true));
		
		String val3 = answers.get(Question.DEFAULT_KEY);
		assertThat(val3, is("testarg"));
	}
	
	@Test
	public void multipleNamedAndDefaultDefaultArg() {
		Answers answers = Answers.parse(Arrays.asList(
				new StringQuestion("a", "test", "default"), 
				new BooleanQuestion("b", "test2"), 
				new StringQuestion(Question.DEFAULT_KEY, "test3", "d")), "-a test", "-b");
		String val = answers.get("a");
		assertThat(val, is("test"));
		
		boolean val2 = answers.get("b");
		assertThat(val2, is(true));
		
		String val3 = answers.get(Question.DEFAULT_KEY);
		assertThat(val3, is("d"));
	}
	
	@Test
	public void relativePath() {
		String tmpDirStr = System.getProperty("java.io.tmpdir");
		Path tmpDir = Paths.get(tmpDirStr);
		
		List<Question<?>> questions = Arrays.asList(
				new PathQuestion(tmpDir, Question.DEFAULT_KEY, "test"));
		Answers answers = Answers.parse(questions, "test");

		Path path = answers.get(Question.DEFAULT_KEY);
		assertThat(path.toAbsolutePath().toString(), is(tmpDirStr + "test"));
	}
	
	@Test
	public void absolutePath() {
		String tmpDirStr = System.getProperty("java.io.tmpdir");
		Path tmpDir = Paths.get(tmpDirStr, "basedir");
		
		List<Question<?>> questions = Arrays.asList(
				new PathQuestion(tmpDir, Question.DEFAULT_KEY, "test"));
		Answers answers = Answers.parse(questions, tmpDirStr + File.separator + "other");

		Path path = answers.get(Question.DEFAULT_KEY);
		assertThat(path.toAbsolutePath().toString(), is(tmpDirStr + "other"));
	}
}
