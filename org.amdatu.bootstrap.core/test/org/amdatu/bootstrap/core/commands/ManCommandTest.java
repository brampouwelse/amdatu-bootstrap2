package org.amdatu.bootstrap.core.commands;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.CommandNotFoundException;
import org.amdatu.bootstrap.core.CommandRegistry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;

@RunWith(MockitoJUnitRunner.class)
public class ManCommandTest {
	@Spy @InjectMocks 
	private ManCommand m_cmd = new ManCommand();
	
	@Mock 
	private CommandRegistry m_registry;
	
	@Before
	public void setup() {
		OngoingStubbing<Command<?>> whenMan = when(m_registry.getCommand("bootstrap-man"));
		whenMan.thenReturn(new ManCommand());
		
		OngoingStubbing<Command<?>> whenLs = when(m_registry.getCommand("bootstrap-ls"));
		whenLs.thenReturn(new LsCommand());
		
		OngoingStubbing<Command<?>> whenUnknown = when(m_registry.getCommand("unknown"));
		whenUnknown.thenThrow(new CommandNotFoundException("unknown"));
	}
	
	@Test
	public void withoutArgs() {
		String result = m_cmd.execute();
		assertThat(result.contains("usage: bootstrap-man"), is(true));
	}
	
	@Test
	public void withCommandNameArg() {
		String result = m_cmd.execute("bootstrap-ls");
		assertThat(result.contains("usage: bootstrap-ls"), is(true));
	}

	@Test(expected=CommandNotFoundException.class)
	public void withInvliadCommandNameArg() {
		m_cmd.execute("unknown");
	}
}
