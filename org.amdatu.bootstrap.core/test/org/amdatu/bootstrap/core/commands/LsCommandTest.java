package org.amdatu.bootstrap.core.commands;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.List;

import org.amdatu.bootstrap.core.Navigator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LsCommandTest {
	
	@Spy @InjectMocks LsCommand cmd = new LsCommand();
	
	@Mock Navigator m_navigator;
	
	@Before
	public void setup() {
		when(m_navigator.getCurrentDir()).thenReturn(new File( System.getProperty("user.dir"), "test/ls-testdir").toPath());
	}
	
	@Test
	public void withoutArgs() {
		List<File> result = cmd.execute();
		assertThat(result.size(), is(4));
	}

	@Test
	public void recursive() {
		List<File> result = cmd.execute("-R");
		assertThat(result.size(), is(8));
		
	}
	
	@Test
	public void files() {
		List<File> result = cmd.execute("-F");
		assertThat(result.size(), is(2));
	}
	
	@Test
	public void recursiveFilesOnly() {
		List<File> result = cmd.execute("-R","-F");
		assertThat(result.size(), is(6));
	}
	
	@Test
	public void name() {
		List<File> result = cmd.execute("file1.*");
		assertThat(result.size(), is(1));
	}
	
	@Test
	public void combined() {
		List<File> result = cmd.execute("-R", "-F", ".*file1.*");
		assertThat(result.size(), is(3));
	}
}
