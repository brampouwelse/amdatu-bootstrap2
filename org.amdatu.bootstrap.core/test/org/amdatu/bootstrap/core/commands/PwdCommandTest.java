package org.amdatu.bootstrap.core.commands;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.io.File;
import java.nio.file.Path;

import org.amdatu.bootstrap.core.Navigator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PwdCommandTest {
	@Spy
	@InjectMocks
	private PwdCommand m_cmd = new PwdCommand();

	@Mock
	Navigator m_navigator;

	private Path m_path;

	@Before
	public void setup() {
		m_path = new File(System.getProperty("user.dir")).toPath();
		when(m_navigator.getCurrentDir()).thenReturn(m_path);
	}

	@Test
	public void pwd() {
		File result = m_cmd.execute();
		assertThat(result, is(m_path.toFile()));
	}

}
