package org.amdatu.bootstrap.core.commands;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.CommandRegistry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ListCommandsCommandTest {
	@Spy @InjectMocks 
	private ListCommandsCommand m_cmd;
	
	@Mock 
	private CommandRegistry m_registry;
	
	@Before
	public void setup() {
		when(m_registry.listCommands()).thenReturn(Arrays.asList(new LsCommand(), new PwdCommand()));
	}
	
	@Test
	public void listAll() {
		List<Command<?>> result = m_cmd.execute();
		assertThat(result.size(), is(2));
	}

	@Test
	public void listFiltered() {
		List<Command<?>> result = m_cmd.execute("-n bootstrap-l.*");
		assertThat(result.size(), is(1));
	}
}
