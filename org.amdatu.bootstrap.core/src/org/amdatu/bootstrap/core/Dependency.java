/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a dependency in a workspace and/or project, containing a BSN
 * (bundle symbolic name) and an optional version.
 */
public class Dependency {
    private final String m_bsn;
    private final String m_version;

    /**
     * Creates a new {@link Dependency} instance for a string representation of
     * a dependency. This string representation has the following form (in
     * pseudo BNF):
     * 
     * <pre>
     * dependency ::= bsn ( ';' arg )*
     * arg ::= 'version' '=' vsn
     * </pre>
     * <p>
     * Where the <tt>bsn</tt> and <tt>vsn</tt> are the string representations of
     * the bundle symbolic name and version as defined in the OSGi core
     * specification.
     * </p>
     * 
     * @param dep
     *        the string representation of the bundle symbolic name, and
     *        optional version, of this dependency, cannot be
     *        <code>null</code> or empty.
     * @throws IllegalArgumentException
     *         in case the given BSN was <code>null</code> or empty.
     */
    public Dependency(String dep) {
        if (dep == null || "".equals(dep.trim())) {
            throw new IllegalArgumentException("Dependency cannot be null or empty!");
        }

        int bsnIdx = -1;
        int vsnIdx = -1;

        String[] parts = dep.split("\\s*;\\s*");
        for (int i = 0; i < parts.length; i++) {
            if ((bsnIdx >= 0) && parts[i].startsWith("version")) {
                if (vsnIdx >= 0) {
                    throw new IllegalArgumentException("Invalid dependency: " + dep + ". Multiple versions defined?!");
                }
                vsnIdx = i;
            }
            else if (parts[i].indexOf('=') < 0) {
                if (bsnIdx >= 0) {
                    throw new IllegalArgumentException("Invalid dependency: " + dep + ". Multiple BSNs defined?!");
                }
                bsnIdx = i;
            }
        }

        if (bsnIdx < 0) {
            throw new IllegalArgumentException("Invalid dependency: " + dep + ". No BSN defined?!");
        }

        m_bsn = parts[bsnIdx];
        m_version = (vsnIdx < 0) ? null : getArgValue(parts[vsnIdx]).replaceAll("'", "");
    }

    /**
     * Creates a new {@link Dependency} instance with a given BSN and (optional) version.
     * 
     * @param bsn
     *        the bundle symbolic name of this dependency, cannot be
     *        <code>null</code> or empty;
     * @param version
     *        the optional version of this dependency, can be
     *        <code>null</code> or empty in case no particular version is
     *        desired.
     * @throws IllegalArgumentException
     *         in case the given BSN was <code>null</code> or empty.
     */
    public Dependency(String bsn, String version) {
        if (bsn == null || "".equals(bsn.trim())) {
            throw new IllegalArgumentException("BSN cannot be null or empty!");
        }
        if (bsn.indexOf(';') >= 0) {
            throw new IllegalArgumentException("Arguments not allowed in the BSN!");
        }

        m_bsn = bsn;
        m_version = trim(version);
    }

    /**
     * Convenience method to create one or more dependencies from a string
     * representation.
     * 
     * @param dependencies
     *        the string representations of the dependencies to instantiate,
     *        can be an empty array if no dependencies are desired.
     * @return a list with dependencies, in the same order as the given
     *         arguments, never <code>null</code>.
     * @see #Dependency(String)
     */
    public static List<Dependency> fromStrings(String... dependencies) {
        List<Dependency> result = new ArrayList<>();
        for (String dependency : dependencies) {
            result.add(new Dependency(dependency));
        }
        return result;
    }

    private static String trim(String value) {
        if (value == null) {
            return null;
        }
        value = value.trim();
        if ("".equals(value)) {
            return null;
        }
        return value;
    }

    private static String getArgValue(String value) {
        String[] parts = value.split("\\s*=\\s*");
        if (parts.length < 2) {
            throw new IllegalArgumentException("Expected value for " + parts[0]);
        }
        return parts[1].trim();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Dependency other = (Dependency) obj;
        if (!m_bsn.equals(other.m_bsn)) {
            return false;
        }
        if (m_version == null) {
            if (other.m_version != null) {
                return false;
            }
        }
        else if (!m_version.equals(other.m_version)) {
            return false;
        }
        return true;
    }

    public String getBsn() {
        return m_bsn;
    }

    public String getVersion() {
        return m_version;
    }

    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 1;
        result = prime * result + ((m_bsn == null) ? 0 : m_bsn.hashCode());
        result = prime * result + ((m_version == null) ? 0 : m_version.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Dependency [bsn=" + m_bsn + ", version=" + (m_version == null ? "<NO VERSION>" : m_version) + "]";
    }
}
