package org.amdatu.bootstrap.core;

import org.amdatu.bootstrap.core.questions.Question;

public class RequiredArgumentNotSet extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public RequiredArgumentNotSet(Question<?> question) {
		super("Required argument -" + question.getKey() + "not set");
	}
}
