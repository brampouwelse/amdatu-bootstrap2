package org.amdatu.bootstrap.core;

import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.questions.Question;

public interface Command<T> {
	String getName();
	Plugin getPlugin();
	Scope getScope();
	List<Question<?>> getQuestions();
	T execute(String... args);
	
	default T _main(String... args){
		if (args != null && args.length > 1){
			args = Arrays.copyOfRange(args, 1, args.length);
			return execute(args);
		}
		
		return execute();
	}
	
	default String getFullName() {
		return getPlugin().getName() + ":" + getName();
	}
}
