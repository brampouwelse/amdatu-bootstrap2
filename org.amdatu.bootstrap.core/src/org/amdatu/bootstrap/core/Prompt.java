package org.amdatu.bootstrap.core;

import org.amdatu.bootstrap.core.questions.Question;

public interface Prompt {

	void ask(Question<?> question);

}
