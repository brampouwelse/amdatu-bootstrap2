package org.amdatu.bootstrap.core;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

import aQute.bnd.build.Project;
import aQute.bnd.build.Workspace;

public interface Navigator {

	Workspace getCurrentWorkspace();

	Project getCurrentProject();

	Path getCurrentDir();

	void changeDir(Path newDir);

	Path getProjectDir();

	Path getWorkspaceDir();

	List<Path> getBndRunFiles();

	List<File> listProjectBndFiles();

	List<Path> findWorkspaceRunConfigs();

	Path getHomeDir();

	Path getPreviousDir();
	
	Path getBndFile();
}
