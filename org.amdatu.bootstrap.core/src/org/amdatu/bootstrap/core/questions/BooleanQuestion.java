package org.amdatu.bootstrap.core.questions;

public class BooleanQuestion extends Question<Boolean>{

	public BooleanQuestion(String key, String label, boolean defaultValue) {
		super(key, label, defaultValue);
	}
	
	public BooleanQuestion(String key, String label) {
		super(key, label, null);
	}

	@Override
	public void setAnswerAsString(String answer) {
		this.answer = Boolean.parseBoolean(answer);
	}

}
