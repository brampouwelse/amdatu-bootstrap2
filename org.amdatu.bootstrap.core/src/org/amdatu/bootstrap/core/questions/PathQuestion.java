package org.amdatu.bootstrap.core.questions;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathQuestion extends Question<Path> {
	private final Path m_basePath;
	
	public PathQuestion(Path basePath, String key, String label, Path defaultValue) {
		super(key, label, defaultValue);
		m_basePath = basePath;
	}
	
	public PathQuestion(Path basePath, String key, String label) {
		super(key, label, null);
		m_basePath = basePath;
	}
	
	@Override
	public void setAnswerAsString(String answer) {
		Path path = Paths.get(answer);
		if(path.isAbsolute()) {
			this.answer = path;
		} else {
			this.answer = m_basePath.resolve(path);
		}
	}
}
