package org.amdatu.bootstrap.core.questions;

import java.util.List;

public class ChoiceQuestion <T> extends Question<T>{
	private final List<T> options;
	private final int defaultChoice;

	
	public ChoiceQuestion(String key, String label, List<T> options, int defaultValue) {
		super(key, label, options.get(defaultValue));
		
		this.options = options;
		this.defaultChoice = defaultValue;
	}

	public ChoiceQuestion(String key, String label, List<T> options) {
		super(key, label, null);
		
		this.options = options;
		this.defaultChoice = 0;
	}

	public List<T> getOptions() {
		return options;
	}

	public int getDefaultChoice() {
		return defaultChoice;
	}

	@Override
	public void setAnswerAsString(String answer) {
		try {
			int parseInt = Integer.parseInt(answer);
			this.answer = getOptions().get(parseInt);
		} catch(NumberFormatException ex) {
			for(T option : getOptions()) {
				if(option.toString().equals(answer)) {
					this.answer = option;
					return;
				} 
			}
			
//			if(this.answer == null) {
//				throw new IllegalArgumentException("Option " + answer + " not found");
//			}
		}
	}
}
