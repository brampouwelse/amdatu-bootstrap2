package org.amdatu.bootstrap.core.questions;


public class StringQuestion extends Question<String >{

	public StringQuestion(String key, String label, String defaultValue) {
		super(key, label, defaultValue);
	}
	
	public StringQuestion(String key, String label) {
		super(key, label, null);
	}

	@Override
	public void setAnswerAsString(String answer) {
		this.answer = answer.trim();
	}
}
