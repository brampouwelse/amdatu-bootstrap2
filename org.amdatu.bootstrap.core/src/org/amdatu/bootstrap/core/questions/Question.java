package org.amdatu.bootstrap.core.questions;

/**
 * Prompt questions are implemented as concrete extensions of this class. Due to the limited generics model in Java it's impossible to
 * create an API that doesn't need casting on the user's side without using sub classes. 
 */
public abstract class Question<T> {
	public final static String DEFAULT_KEY = "default-key";	
	private final String key;
	private final String label;
	private final T defaultValue;
	protected T answer;

	protected Question(String key, String label, T defaultValue) {
		this.key = key;
		this.label = label;
		this.defaultValue = defaultValue;
	}
	
	public String getLabel() {
		return label;
	}

	public T getDefaultValue() {
		return defaultValue;
	}
	
	public T getAnswer() {
		return answer;
	}

	public abstract void setAnswerAsString(String answer);
	
	public String getKey() {
		return key;
	}
	
	public boolean isDefaultArg() {
		return key.equals(DEFAULT_KEY);
	}
	
	public boolean isNamedArg() {
		return !key.equals(DEFAULT_KEY);
	}
}
