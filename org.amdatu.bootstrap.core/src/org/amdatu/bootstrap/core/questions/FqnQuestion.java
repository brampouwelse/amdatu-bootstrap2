package org.amdatu.bootstrap.core.questions;


public class FqnQuestion extends Question<FullyQualifiedName> {

	public FqnQuestion(String key) {
		super(key, null, null);
	}
	
	public FqnQuestion(String key, String label) {
		super(key, label, null);
	}
	
	public FqnQuestion(String key, String label, FullyQualifiedName defaultValue) {
		super(key, label, defaultValue);
	}

	@Override
	public void setAnswerAsString(String answer) {
		this.answer = new FullyQualifiedName(answer);
	}

}
