package org.amdatu.bootstrap.core;

import java.util.List;

public interface CommandRegistry {
	List<Plugin> listPlugins();
	List<Command<?>> listCommands();
	Command<?> getCommand(String name);
}
