package org.amdatu.bootstrap.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import org.amdatu.bootstrap.core.questions.BooleanQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;

public class Answers {
	private final Map<String, Question<?>> m_questions;

	public Answers(Map<String, Question<?>> questions) {
		m_questions = questions;
	}
	
	private Answers(List<Question<?>> questions, String... args) {
		m_questions = new HashMap<>();
		questions.forEach(q -> m_questions.put(q.getKey(), q));

		Options options = new Options();

		questions.stream()
				.filter(Question::isNamedArg)
				.map(q -> Option.builder(q.getKey())
							.longOpt(q.getKey())
							.hasArg(!(q instanceof BooleanQuestion))
							.required(q.getDefaultValue() == null)
							.desc(q.getLabel()).build())
				.forEach(options::addOption);
		
		
		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse(options, args,true);
			
			m_questions.values().forEach(q -> q.setAnswerAsString(getAnswerAsString(cmd, q)));
			
			Predicate<Question<?>> isDefaultArg = Question::isDefaultArg;
			Predicate<Question<?>> hasNoDefaultValue = q -> q.getDefaultValue() == null;
			
			Optional<Question<?>> defaultArg = questions.stream()
					.filter(isDefaultArg.and(hasNoDefaultValue))
					.findAny();
			
			if(defaultArg.isPresent() && (cmd.getArgs().length == 0 || cmd.getArgs()[0].length() == 0)) {
				throw new RequiredDefaultArgumentNotSet(defaultArg.get());
			}
			
		} catch (ParseException e) {
			throw new ParseExceptionWrapper(e);
		}
	}

	private String getAnswerAsString(CommandLine cmd, Question<?> q) {
		if(q.getKey().equals(Question.DEFAULT_KEY)) {
			return Optional.ofNullable(StringUtils.join(cmd.getArgs())).orElse("" + q.getDefaultValue());
		} else if(q instanceof BooleanQuestion) {
			return Boolean.toString(cmd.hasOption(q.getKey()));
		} else {
			return cmd.getOptionValue(q.getKey(), "" +q.getDefaultValue());
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		Question<?> question = m_questions.get(key);
		Object answer = question.getAnswer();
		
		if(answer == null) {
			return (T)question.getDefaultValue();
		}
		
		if(answer instanceof String) {
			if(((String)answer).length() == 0) {
				return (T)question.getDefaultValue();
			} 
		}
		
		return (T)answer;
	}

	public static Answers parse(List<Question<?>> questions, String... args) {

		return new Answers(questions, args);
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("Answer: \n");

		m_questions.values().forEach(q -> {
			b.append(q.getKey()).append("=").append(q.getAnswer());
		});

		return b.toString();
	}

}
