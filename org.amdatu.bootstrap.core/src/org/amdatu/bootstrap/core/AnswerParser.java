package org.amdatu.bootstrap.core;

import java.util.List;

import org.amdatu.bootstrap.core.questions.Question;

public interface AnswerParser {

	Answers parse(List<Question<?>> questions, String... args);

	void setInteractive(boolean interactive);
	
}
