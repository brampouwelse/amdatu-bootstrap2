package org.amdatu.bootstrap.core;

import org.apache.commons.cli.ParseException;


public class ParseExceptionWrapper extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ParseExceptionWrapper(ParseException ex) {
		super(ex);
	}
}
