/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.amdatu.bootstrap.core.ResourceManager;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class DefaultResourceManager implements ResourceManager {
    @Override
    public void writeFile(Path path, byte[] contents) {
        try {
            Files.write(path, contents, StandardOpenOption.TRUNCATE_EXISTING);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void unzipFile(File fileToUnzip, File toLocation) {
        byte[] buffer = new byte[1024];

        // create output directory is not exists
        if (!toLocation.exists()) {
            toLocation.mkdir();
        }
        // get the zip file content
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(fileToUnzip))) {
            // get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {

                String fileName = ze.getName();
                File newFile = new File(toLocation + File.separator + fileName);

                // create all non exists folders
                // else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                if (ze.isDirectory()) {
                    // create the folder
                    newFile.mkdirs();
                }
                else {
                    // extract file
                    try (FileOutputStream fos = new FileOutputStream(newFile)) {
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                    }
                }

                ze = zis.getNextEntry();
            }

            zis.closeEntry();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        }
        if (!f.delete()) {
            throw new IOException("Failed to delete file: " + f);
        }
    }

    @Override
    public List<File> findFiles(File folderToStartSearching, String what) {
        List<File> result = new ArrayList<File>();
        if (folderToStartSearching.isFile() && folderToStartSearching.getName().equals(what)) {
            result.add(folderToStartSearching);
        }
        if (folderToStartSearching.isDirectory()) {
            for (File f : folderToStartSearching.listFiles()) {
                result.addAll(findFiles(f, what));
            }
        }
        return result;
    }
}
