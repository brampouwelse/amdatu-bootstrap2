package org.amdatu.bootstrap.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import org.amdatu.bootstrap.core.AnswerParser;
import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.ParseExceptionWrapper;
import org.amdatu.bootstrap.core.Prompt;
import org.amdatu.bootstrap.core.RequiredDefaultArgumentNotSet;
import org.amdatu.bootstrap.core.questions.BooleanQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class AnswerParserImpl implements AnswerParser {

	@ServiceDependency
	Prompt m_propmt;
	
	private boolean interactive = true;
	
	@Override
	public Answers parse(List<Question<?>> questions, String... args) {
		Map<String, Question<?>> questionMap = new HashMap<>();
		questions.forEach(q -> questionMap.put(q.getKey(), q));

		Options options = new Options();

		questions
				.stream()
				.filter(Question::isNamedArg)
				.map(q -> Option.builder(q.getKey()).longOpt(q.getKey())
						.hasArg(!(q instanceof BooleanQuestion))
						.required(q.getDefaultValue() == null && !interactive)
						.desc(q.getLabel()).build())
				.forEach(options::addOption);

		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse(options, args, true);

			questionMap.values().forEach(
					q -> q.setAnswerAsString(getAnswerAsString(cmd, q)));

			Predicate<Question<?>> isDefaultArg = Question::isDefaultArg;
			Predicate<Question<?>> hasNoDefaultValue = q -> q.getDefaultValue() == null;

			Predicate<Question<?>> unanswered = new Predicate<Question<?>>() {

				@Override
				public boolean test(Question<?> q) {
					if (q.getDefaultValue() == null) {
						if ((q.isDefaultArg() && cmd.getArgs().length == 0)
								|| (!q.isDefaultArg() && !cmd.hasOption(q
										.getKey()))) {
							return true;
						}
					}
					return false;
				}
			};

			if (interactive) {
				questions.stream().filter(unanswered).forEach(q -> m_propmt.ask(q));
			} else {
				Optional<Question<?>> defaultArg = questions.stream()
						.filter(isDefaultArg.and(hasNoDefaultValue)).findAny();

				if (defaultArg.isPresent()
						&& (cmd.getArgs().length == 0 || cmd.getArgs()[0].length() == 0)) {
					throw new RequiredDefaultArgumentNotSet(defaultArg.get());
				}
			}

			return new Answers(questionMap);
		} catch (ParseException e) {
			throw new ParseExceptionWrapper(e);
		}
	}

	private String getAnswerAsString(CommandLine cmd, Question<?> q) {
		if (q.getKey().equals(Question.DEFAULT_KEY)) {
			return Optional.ofNullable(StringUtils.join(cmd.getArgs())).orElse(
					"" + q.getDefaultValue());
		} else if (q instanceof BooleanQuestion) {
			return Boolean.toString(cmd.hasOption(q.getKey()));
		} else {
			return cmd.getOptionValue(q.getKey(), "" + q.getDefaultValue());
		}
	}
	
	@Override
	public void setInteractive(boolean interactive) {
		this.interactive = interactive;
	}
}
