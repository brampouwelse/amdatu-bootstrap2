package org.amdatu.bootstrap.core.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.CommandNotFoundException;
import org.amdatu.bootstrap.core.CommandRegistry;
import org.amdatu.bootstrap.core.Plugin;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.ServiceReference;

@org.apache.felix.dm.annotation.api.Component
public class CommandRegistryImpl implements CommandRegistry {

	private final Map<ServiceReference<Command<?>>, Command<?>> m_commands = new ConcurrentHashMap<>();

	@ServiceDependency(removed = "removeCommand")
	public void addCommand(ServiceReference<Command<?>> sr, Command<?> command) {
		m_commands.put(sr, command);
	}

	public void removeCommand(ServiceReference<Command<?>> sr,
			Command<?> command) {
		m_commands.remove(sr);
	}

	public List<Plugin> listPlugins() {
		return m_commands.values().stream().map(Command::getPlugin).distinct()
				.sorted((p1, p2) -> p1.getName().compareTo(p2.getName()))
				.collect(Collectors.toList());
	}

	public List<Command<?>> listCommands() {
		return m_commands.values().stream()
				.sorted((c1, c2) -> c1.getName().compareTo(c2.getName()))
				.collect(Collectors.toList());
	}

	@Override
	public Command<?> getCommand(String name) {
		return m_commands.values().stream()
				.filter(c -> (c.getFullName()).equals(name))
				.findFirst().orElseThrow(() -> new CommandNotFoundException(name));
	}
}
