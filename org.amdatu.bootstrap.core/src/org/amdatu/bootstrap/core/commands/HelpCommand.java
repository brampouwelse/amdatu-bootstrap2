package org.amdatu.bootstrap.core.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.CommandRegistry;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.Question;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class HelpCommand implements Command<List<String>>{

	@ServiceDependency
	private volatile CommandRegistry m_commandRegistry;
	
	@Override
	public String getName() {
		return "help";
	}

	@Override
	public Plugin getPlugin() {
		return BootstrapPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.WORKSPACE;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return null;
	}

	@Override
	public List<String> execute(String... args) {
		List<Command<?>> listCommands = m_commandRegistry.listCommands();
		List<String> commands = new ArrayList<>();
		
		for (Command<?> command : listCommands) {
			commands.add(command.getFullName());
		}
		
		Collections.sort(commands);
		
		return commands;
	}

}
