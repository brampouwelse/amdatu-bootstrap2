package org.amdatu.bootstrap.core.commands;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.core.questions.StringQuestion;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class CdCommand implements Command<File> {

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@Override
	public String getName() {
		return "cd";
	}

	@Override
	public Plugin getPlugin() {
		return BootstrapPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.GLOBAL;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(new StringQuestion(Question.DEFAULT_KEY, "directory", "~"));
	}

	@Override
	public File execute(String... args) {
		Answers ansers = Answers.parse(getQuestions(), args);
		String dirName = ansers.get(Question.DEFAULT_KEY);
		Path currentDir = m_navigator.getCurrentDir();
		Path newDir;
		
		if (dirName.equals("..")) {
			newDir = currentDir.getParent();
		}
		else if (dirName.equals("-")) {
			newDir = m_navigator.getPreviousDir();
		}
		else if (dirName.equals("~")) {
			newDir = m_navigator.getHomeDir();
		}
		else if (dirName.startsWith("~" + File.separator)) {
	        newDir = m_navigator.getHomeDir().resolve(dirName.substring(2));
		}
		else {
		    newDir = currentDir.resolve(dirName);
		}
		
		if (newDir.toFile().exists() && newDir.toFile().isDirectory()) {
			m_navigator.changeDir(newDir);
			return newDir.toFile();
		}
		else {
			throw new IllegalArgumentException("Invalid directory " + newDir);
		}
	}

}
