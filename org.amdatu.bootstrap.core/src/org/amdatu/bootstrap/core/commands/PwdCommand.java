package org.amdatu.bootstrap.core.commands;

import java.io.File;
import java.util.List;

import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.Question;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class PwdCommand implements Command<File>{

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@Override
	public String getName() {
		return "pwd";
	}

	@Override
	public Plugin getPlugin() {
		return BootstrapPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.GLOBAL;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return null;
	}

	@Override
	public File execute(String... args) {
		return m_navigator.getCurrentDir().toFile();
	}

}
