package org.amdatu.bootstrap.core.commands;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.BooleanQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.core.questions.StringQuestion;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class LsCommand implements Command<List<File>> {
	@ServiceDependency
	private volatile Navigator m_navigator;

	@Override
	public String getName() {
		return "ls";
	}

	@Override
	public Plugin getPlugin() {
		return BootstrapPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.GLOBAL;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(
				new BooleanQuestion("R", "recursive", false),
				new BooleanQuestion("F", "files only", false),
				new StringQuestion(Question.DEFAULT_KEY, "filter", ""));
	}

	@Override
	public List<File> execute(String... args) {
		Answers answers = Answers.parse(getQuestions(), args);
		
		
		FileFilter filter = null;
		boolean recursive = answers.get("R");
		boolean filesOnly = answers.get("F");
		String nameFilter = answers.get(Question.DEFAULT_KEY);
		if(nameFilter.length() > 0) {
			
			filter = f -> f.getName().matches(nameFilter) && (f.isFile() || !filesOnly); 
		}
		
		File dir = m_navigator.getCurrentDir().toFile();
		File[] files;
		if (recursive) {
			return listRecursiveFiles(dir, filter, filesOnly);
		}
		else {
			files = (filter == null ? dir.listFiles(f -> f.isFile() || !filesOnly) : dir.listFiles(filter));
			return Arrays.asList(files);
		}
	}

	private List<File> listRecursiveFiles(File dir, FileFilter filter,
			boolean filesOnly) {
		List<File> list = new ArrayList<>();
		listRecursiveFiles(dir, list, filter, filesOnly);
		return list;
	}

	private void listRecursiveFiles(File dir, List<File> list,
			FileFilter filter, boolean filesOnly) {
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				listRecursiveFiles(file, list, filter, filesOnly);
				if (!filesOnly) {
					list.add(file);
				}
			}
		}
		files = (filter == null ? dir.listFiles() : dir.listFiles(filter));
		for (File file : files) {
			if (file.isFile()) {
				list.add(file);
			}
		}
	}

}
