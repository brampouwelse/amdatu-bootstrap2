package org.amdatu.bootstrap.core.commands;

import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.AnswerParser;
import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.BooleanQuestion;
import org.amdatu.bootstrap.core.questions.Question;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class AnswerParserInteractiveCommand implements Command<Void>{

	@ServiceDependency
	private volatile AnswerParser m_answerService;
	
	@Override
	public String getName() {
		return "interactive";
	}

	@Override
	public Plugin getPlugin() {
		return BootstrapPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.GLOBAL;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(new Question[]{new BooleanQuestion(Question.DEFAULT_KEY, "Enable interactive answers", true)});
	}

	@Override
	public Void execute(String... args) {
		Answers answers = m_answerService.parse(getQuestions(), args);
		System.out.println("interactive " + answers.get(Question.DEFAULT_KEY));
		
		m_answerService.setInteractive(answers.get(Question.DEFAULT_KEY));
		return null;
	}

}
