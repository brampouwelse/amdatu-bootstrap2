package org.amdatu.bootstrap.core.commands;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.CommandRegistry;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.core.questions.StringQuestion;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class ManCommand implements Command<String> {
	
	@ServiceDependency
	private volatile CommandRegistry m_registry;
	
	@Override
	public String getName() {
		return "man";
	}

	@Override
	public Plugin getPlugin() {
		return BootstrapPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.GLOBAL;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(new StringQuestion(Question.DEFAULT_KEY, "Command name", "bootstrap-man"));
	}

	@Override
	public String execute(String... args) {
		Answers answers = Answers.parse(getQuestions(), args);
		String commandName = answers.get(Question.DEFAULT_KEY);
		Command<?> command = m_registry.getCommand(commandName);
		
		Options options = new Options();
		
		if(command.getQuestions() != null) {
			command.getQuestions().stream()
					.filter(Question::isNamedArg)
					.map(q -> Option.builder(q.getKey()).
							longOpt(q.getKey())
							.hasArg()
							.required(q.getDefaultValue() == null)
							.desc(q.getLabel()).build())
					.forEach(options::addOption);
		}
		
		HelpFormatter f = new HelpFormatter();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
        
		try(PrintWriter writer = new PrintWriter(out)) {
			StringBuilder sb = new StringBuilder(command.getFullName());
			
			if(command.getQuestions() != null) {
				Optional<Question<?>> defaultArgs = command.getQuestions()
						.stream()
						.filter(Question::isDefaultArg)
						.findAny();
			
				if(defaultArgs.isPresent()) {
					sb.append(" [").append(defaultArgs.get().getLabel()).append("]");
				}
			}
			
			f.printHelp(writer, 200, sb.toString(), null, options, 0,0, null);
		}
        
		String string = new String(out.toByteArray());
		return string;
	}

}
