package org.amdatu.bootstrap.core.commands;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.core.Answers;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.CommandRegistry;
import org.amdatu.bootstrap.core.Plugin;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.core.questions.Question;
import org.amdatu.bootstrap.core.questions.StringQuestion;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class ListCommandsCommand implements Command<List<Command<?>>>{
	
	@ServiceDependency
	private volatile CommandRegistry m_registry;
	
	@Override
	public String getName() {
		return "list-commands";
	}

	@Override
	public Plugin getPlugin() {
		return BootstrapPlugin.INSTANCE;
	}

	@Override
	public Scope getScope() {
		return Scope.GLOBAL;
	}

	@Override
	public List<Question<?>> getQuestions() {
		return Arrays.asList(new StringQuestion("n", "Name pattern", ""));
	}

	@Override
	public List<Command<?>> execute(String... args) {
		Answers answers = Answers.parse(getQuestions(), args);
		String nameFilter = answers.get("n");
		
		if(nameFilter.length() > 0) {
			return m_registry.listCommands().stream()
					.filter(c -> c.getFullName().matches(nameFilter))
					.collect(Collectors.toList());
		} else {
			return m_registry.listCommands();
		}
	}

}
