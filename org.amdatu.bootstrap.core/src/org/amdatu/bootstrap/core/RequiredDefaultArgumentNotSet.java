package org.amdatu.bootstrap.core;

import org.amdatu.bootstrap.core.questions.Question;

public class RequiredDefaultArgumentNotSet extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public RequiredDefaultArgumentNotSet(Question<?> question) {
		super("Required argument [" + question.getLabel() + "] not set");
	}
	
}
