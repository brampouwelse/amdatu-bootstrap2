package org.amdatu.bootstrap.core;

public interface Plugin {
	String getName();
	String getAuthor();
	String getGitUrl();
	String getWebsite();
}
